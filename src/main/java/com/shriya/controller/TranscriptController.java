package com.shriya.controller;

import com.opencsv.exceptions.CsvValidationException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(path = "/transcript")
public class TranscriptController {

    @GetMapping(path = "/getSentimentsNumber")
    public SimpleSentimentCount hello() throws IOException, CsvValidationException {
        SimpleSentimentCount count=new SimpleSentimentCount();

        Transcripts transcripts = new Transcripts();
        int[] data =transcripts.getTranscripts();
        count.PositiveSentiments=data[0];
        count.NegativeSentiments=data[1];

        return count;
    }

    /*@PostMapping(path = "/postmessage")
    public String postMessage(@RequestBody SimpleMessagePojo messagePojo){

        System.out.println(messagePojo.getName());
        System.out.println(messagePojo.getAge());

        return messagePojo.getName();
    }*/
}
