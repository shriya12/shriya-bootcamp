package com.shriya.controller;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class Transcripts {
    public int[] getTranscripts() throws IOException, CsvValidationException {
        readSentiments sentiments = new readSentiments();
        HashMap<String, String> sentimentsMaps = sentiments.Sentiments();
        String dir = System.getProperty("user.dir");
        FileReader transcriptsReader = new FileReader(dir+"/transcripts");
        CSVReader csvReader = new CSVReader(transcriptsReader);
        String[] nextLine;
        //File transcriptsFile = new File(dir+"/transcripts");
        //Scanner sc= new Scanner(transcriptsFile);
        int pos=0;
        int neg=0;
        while((nextLine = csvReader.readNext()) != null){

            for(Map.Entry<String,String> entryList:sentimentsMaps.entrySet()){
                if (nextLine[0].contains(entryList.getKey())){
                    if(entryList.getValue().equals("positive")){
                        pos++;

                    }
                    else if(entryList.getValue().equals("negative")){
                        neg++;
                    }

                }
            }

        }
        System.out.println("Positive Sentiments: "+pos);
        System.out.println("Negative Sentiments: "+neg);


        int arr[] = {pos, neg};
        return arr;

    }
}
