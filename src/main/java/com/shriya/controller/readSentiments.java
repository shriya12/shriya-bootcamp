package com.shriya.controller;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;


public class readSentiments {
    public HashMap<String, String> Sentiments() {

        HashMap<String, String> sentimentsMaps = null;
        try {
            String dir = System.getProperty("user.dir");
            FileReader filereader = new FileReader(dir + "/sentiments");
            CSVReader csvReader = new CSVReader(filereader);
            String[] nextRecord;
            sentimentsMaps = new HashMap<>();
            //File f = new File(dir + "/sentiments");
            //Scanner sc = new Scanner(f);
            while ((nextRecord = csvReader.readNext()) != null) {
                sentimentsMaps.put(nextRecord[0], nextRecord[1]);

                //System.out.println(sentiments);
                System.out.println("Key= " + nextRecord[0] + ", Value= " + nextRecord[1]);


            }
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        } catch (CsvValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sentimentsMaps;
    }
}
