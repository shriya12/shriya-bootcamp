package com.shriya;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;



import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class exercise {
    public static void main(String[] args) throws IOException, CsvValidationException {
        String dir = System.getProperty("user.dir");
        FileReader filereader = new FileReader(dir+"/sentiments");
        CSVReader csvReader = new CSVReader(filereader);
        //File f = new File(dir+"/sentiments");
        //Scanner sc = new Scanner(f);
        String[] nextRecord;
        HashMap<String,String> sentimentsMaps =new HashMap<>();
        while((nextRecord = csvReader.readNext()) != null){
            sentimentsMaps.put(nextRecord[0],nextRecord[1]);
        }

        FileReader transcriptsReader = new FileReader(dir+"/transcripts");
        csvReader = new CSVReader(transcriptsReader);
        //File transcriptsFile = new File(dir+"/transcripts");
        String[] nextLine;
        int pos=0;
        int neg=0;
        while((nextLine = csvReader.readNext()) != null){
            for(Map.Entry<String,String> entryList:sentimentsMaps.entrySet()){
                if (nextLine[0].contains(entryList.getKey())){
                    if(entryList.getValue().equals("positive")){
                        pos++;

                    }
                    else if(entryList.getValue().equals("negative")){
                        neg++;
                    }

                }
            }

        }
        System.out.println("Positive sentiment Transcripts: " +pos);
        System.out.println("Negative sentiment Transcripts: " +neg);


    }
}
